import sys
import datetime
from time import time
import keras
from numpy import zeros
from numpy import ones
from numpy import vstack
from numpy.random import randn
import numpy
from keras.optimizers import Adam, Nadam, SGD
from keras.models import Sequential
from keras.layers import RNN, LSTM, InputLayer, GRU
from keras.layers import Dense
from keras.layers import Reshape
import flows as fl
import pickle

numpy.random.seed(1)

batch_size = 256
n_epoch = 40
optimaizer="nadam"
learning_rateD = 0.00003
learning_rateG = 0.000005
loss="binary_crossentropy"
d_repets = 1
smoot_labeling=False
gen_start=3
beta1=0.9
metrics="accuracy"
momentum=0.3
data_from_existing_model=False
if len(sys.argv) >6:
    n_epoch = int(sys.argv[1])
    optimaizer = sys.argv[2]
    learning_rateD = float(sys.argv[3])
    learning_rateG = float(sys.argv[4])
    loss = sys.argv[5]
    d_repets = int(sys.argv[6])
    smoot_labeling = bool(sys.argv[7])
    gen_start = int(sys.argv[8])
if len(sys.argv)>9:
    beta1=float(sys.argv[9])

if len(sys.argv)>10:
    metrics=sys.argv[10]

if len(sys.argv)>11:
    momentum=float(sys.argv[11])

if len(sys.argv)>12:
    data_from_existing_model= bool(sys.argv[12])

# define the standalone discriminator model
def define_discriminator(in_shape=(batch_size, 2)):
    model = Sequential()
    model.add(LSTM(32, input_shape=in_shape))
    model.add(Dense(512, activation='elu'))
    model.add(Dense(512, activation='elu'))
    model.add(Dense(512, activation='elu'))
    model.add(Dense(1, activation='elu'))
    # compile model
    if optimaizer=='nadam':
        opt = Nadam(lr=learning_rateD,beta_1=beta1)
    elif optimaizer=='adam':
        opt = Adam(lr=learning_rateD, beta_1=beta1)
    else:
        opt = SGD(lr=learning_rateD,momentum=0.3)
    model.compile(loss=loss, optimizer=opt, metrics=[metrics])
    return model


# define the standalone generator model
def define_generator():
    in_shape = (batch_size, 2)
    model = Sequential()
    model.add(InputLayer(input_shape=in_shape))
    model.add(LSTM(32, input_shape=in_shape))
    model.add(Dense(512, activation='elu'))
    model.add(Dense(512, activation='elu'))
    model.add(Dense(512, activation='elu'))
    model.add(Dense(512, activation='elu'))
    model.add(Reshape(in_shape))
    # compile model
    # if optimaizer == 'nadam':
    #     opt = Nadam(lr=learning_rateG,beta_1=beta1)
    # elif optimaizer == 'adam':
    #     opt = Adam(lr=learning_rateG,beta_1=beta1)
    # else:
    #     opt = SGD(lr=learning_rateG,momentum=momentum)
    # model.compile(loss=loss, optimizer=opt, metrics=[metrics])
    return model


# define the combined generator and discriminator model, for updating the generator
def define_gan(g_model, d_model):
    # make weights in the discriminator not trainable
    d_model.trainable = False
    g_model.trainable = True
    # connect them
    model = Sequential()
    # add generator
    model.add(g_model)
    # add the discriminator
    model.add(d_model)
    # compile model
    if optimaizer == 'nadam':
        opt = Nadam(lr=learning_rateG,beta_1=beta1)
    elif optimaizer == 'adam':
        opt = Adam(lr=learning_rateG, beta_1=beta1)
    else:
        opt = SGD(lr=learning_rateG,momentum=0.3)
    model.compile(loss=loss, optimizer=opt,metrics=[metrics])
    return model


# load and prepare mnist training images


# select real samples
def smooth_negative_labels(labels):
    return labels + np.random.random(labels.shape) * 0.3


def generate_fake_samples(gen_model,gan=False):
    gen_model_input_shape = (batch_size, 2)
    noise_shape = (32, *gen_model_input_shape)
    noise = np.random.random(noise_shape)

    if gan:
        return (noise, ones([32, 1]))

    fake_samples = gen_model.predict(noise)
    if smoot_labeling:
        return (fake_samples, smooth_negative_labels(zeros([32, 1])))
    else:
       return (fake_samples, zeros([32, 1]))


# evaluate the discriminator, plot generated images, save generator model
def summarize_performance(epoch, g_model, d_model):
    real_data_test_generator = generate_real_samples_test(dataset2)
    # evaluate discriminator on real examples
    discriminator_acc = []
    generator_acc = []
    for batch_test in range(192):
        X_real, y_real = next(real_data_test_generator)
        if np.isnan(X_real).any():
            continue
        _, acc_real = d_model.test_on_batch(X_real, y_real)
        discriminator_acc.append(acc_real)
        # prepare fake examples
        x_fake, y_fake = generate_fake_samples(g_model)
        # evaluate discriminator on fake examples
        _, acc_fake = d_model.test_on_batch(x_fake, y_fake)
        generator_acc.append(acc_fake)
    # summarize discriminator performance
    print('>Accuracy real: %.0f%%, fake: %.0f%%' % (
        np.average(discriminator_acc) * 100, numpy.average(generator_acc) * 100))
    filename = 'generator_model_%03d.h5' % (epoch + 1)
    g_model.save(filename)
    tmp=int(n_epoch*(3/4))-7
    if epoch > tmp:
        filename1 = 'discriminator_model_%03d.h5' % (epoch + 1)
        d_model.save(filename1)


# train the generator and discriminator
def train(g_model, d_model, gan_model, dataset, n_epochs, n_batch):
    # manually enumerate epochs
    g_loss = 0.0
    d_loss = 0.0
    epoch_number = int(n_epochs*(3/4))
    start_training_time = time()
    print("Start time: ", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    for epoch in range(epoch_number):
        start_epochs_time = time()
        print("Epoch ", epoch + 1, " start time: ", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        real_data_gen = generate_real_samples_traning(dataset)
        # enumerate batches over the training set
        for step in range(n_batch):  # step
            # get randomly selected 'real' samples
            X_real, y_real = next(real_data_gen)
            if (np.isnan(X_real.flatten()).any()):
                continue
            X_fake, y_fake = generate_fake_samples(g_model)
            X, y = vstack((X_real, X_fake)), vstack((y_real, y_fake))
            # update discriminator model weights
            for inner_loop in range(0, d_repets):
                d_loss, _ = d_model.train_on_batch(X, y)

            if epoch > 0:
                X_gan, y_gan = generate_fake_samples(g_model,gan=True)
                g_loss, _ = gan_model.train_on_batch(X_gan, y_gan)
            # summarize loss on this batch
            print('>%d, %d/%d, d=%.9f, g=%.9f' % (epoch + 1, step + 1, n_batch, d_loss, g_loss))
            # evaluate the model performance, sometimes
        summarize_performance(epoch, g_model, d_model)
        print("Epoch ", epoch + 1, " end time: ", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), " ,duration: ",
              datetime.timedelta(seconds=int(time() - start_epochs_time)))
    print("End time: ", datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), " ,Traning duration: ",
          datetime.timedelta(seconds=int(time() - start_training_time)))


from typing import Tuple, List, Union
import pandas as pd
import numpy as np


class EndOfSeriesException(BaseException):
    pass


class Generator:
    """
F    Object of this class generates fixed len batches of series from pandas DataFrame object.
    """

    def __init__(self, data: Union[pd.DataFrame, List], window_size=256, batch_size=1, batch_info=False):
        self.data = [data, ] if isinstance(data, pd.DataFrame) else data  # TODO -> eliminate checking type
        self.window_size = window_size
        self.index = 0
        self.row = 0
        self.current_series = None
        self.batch_size = batch_size
        self.batch_info = batch_info
        self._get_current_series()

    def __iter__(self):
        return self

    def __next__(self) -> np.array:
        return self._generate_batch()

    def _generate_sample(self) -> np.array:
        while True:
            start, stop = self._get_start_stop_indexes()

            if not self._check_indexes(stop):
                raise EndOfSeriesException()

            self.index += 1
            return np.array([single_series[start:stop].values for single_series in self.current_series]).T

    def _generate_batch(self) -> np.array:
        while True:
            batch = []
            for i in range(self.batch_size):
                try:
                    batch.append(self._generate_sample())
                except EndOfSeriesException:
                    self._move_to_the_next_series()
                    break  # for
                except Exception as e:
                    print(e)
                    raise
            else:
                if not self.batch_info:
                    return np.array(batch)
                else:
                    return np.array(batch), self.batch_info

    def _get_current_series(self) -> None:
        try:
            self.current_series = [single_data.iloc[self.row] for single_data in self.data]

            if self.batch_info:
                self.batch_info = self.data[0].index[self.row]
        except IndexError:
            raise StopIteration

        self._analyse_current_series()
        self._preprocess_series()

    def _preprocess_series(self):
        self._remove_nans_from_the_end_of_series()
        self._remove_nans_from_the_beginning_of_series()
        self._set_proper_len_of_series()

    def _get_start_stop_indexes(self) -> Tuple[int, int]:
        return self.index, self.index + self.window_size

    def _remove_nans_from_the_end_of_series(self) -> None:
        self.current_series = [single_series[:self.end_of_current_series + 1] for single_series in self.current_series]

    def _remove_nans_from_the_beginning_of_series(self) -> None:
        self.current_series = [single_series[self.start_of_current_series:] for single_series in self.current_series]

    def _pad_series_to_match_batch_size(self, len_of_remaining_series: int) -> None:
        padding_len = self.batch_size - len_of_remaining_series
        self.current_series = [pd.Series([0] * padding_len).append(single_series)
                               for single_series in self.current_series]

    def _cut_series_to_match_batch_size(self, start: int) -> None:
        self.current_series = [single_series[start:] for single_series in self.current_series]

    def _set_proper_len_of_series(self) -> None:
        num_of_windows = self.current_series[0].shape[0] - self.window_size + 1
        num_of_batches, rest = divmod(num_of_windows, self.batch_size)

        if num_of_batches < 0:
            return  # do nothing - checking indexes will trigger moving to the next series

        self._cut_series_to_match_batch_size(rest)

    def _check_indexes(self, stop_idx: int) -> bool:
        return all(stop_idx <= len(single_series) for single_series in self.current_series)

    def _move_to_the_next_series(self) -> None:
        self.row += 1
        self.index = 0
        self._get_current_series()

    def _analyse_current_series(self):
        try:
            self.end_of_current_series = min(single_series.last_valid_index()
                                             for single_series in self.current_series)
            self.start_of_current_series = max(single_series.first_valid_index()
                                               for single_series in self.current_series)
        except TypeError:  # if one of item is None (there is no valid index)
            self.end_of_current_series = -1
            self.start_of_current_series = -1


def smooth_positive_labels(labels):
    return labels - 0.3 + (np.random.random(labels.shape) * 0.5)


def generate_real_samples_traning(dataset):
    while True:
        generator = Generator(dataset, batch_size=32,
                              window_size=256)
        for samples in generator:
            labels = np.ones([samples.shape[0], 1])
            if smoot_labeling:
                labels_after = smooth_positive_labels(labels)
            else:
                labels_after = labels
            yield (samples, labels_after)


def generate_real_samples_test(dataset):
    while True:
        generator = Generator(dataset, batch_size=32,
                              window_size=256)
        for samples in generator:
            yield (samples, np.ones([samples.shape[0], 1]))


def load_real_samples(first_pacient=0, last_pacient=50):
    df_hr, df_mbp = fl.load_data()
    # remove outliers
    df_hr = fl.remove_outliers_complete(df_hr)
    df_mbp = fl.remove_outliers_complete(df_mbp)
    # normalize
    df_hr, _ = fl.normalize(df_hr)
    df_mbp, _ = fl.normalize(df_mbp)

    return df_hr.iloc[first_pacient:last_pacient], df_mbp.iloc[first_pacient:last_pacient]  # wzięcie 50 pierwszych pacjentów


if data_from_existing_model:
    with open("../analiza4/model_g.h5", 'rb') as g_model_to_load:
        g_model = pickle.load(g_model_to_load)
    with open("../analiza4/model_d.h5", 'rb') as d_model_to_load:
        d_model = pickle.load(d_model_to_load)

else:
    # create the discriminator
    d_model = define_discriminator()
    # create the generator
    g_model = define_generator()
    # create the gan

gan_model = define_gan(g_model, d_model)

# train data
dataset = load_real_samples()

# test data
dataset2 = load_real_samples(50, 60)

train(g_model, d_model, gan_model, dataset, n_epochs=n_epoch, n_batch=int(batch_size * 2))
pickle.dump(d_model, open("models/model_d.h5", 'wb'))
pickle.dump(g_model, open("models/model_g.h5", 'wb'))
