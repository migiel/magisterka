from scipy.stats import zscore
import pandas as pd
import numpy as np
import flows as fl

def remove_outliers(data: pd.DataFrame, accepted_zscore: float = 3, window_len=256):
    data = data.copy()
    median = data.rolling(window_len, axis=1, center=True).median().fillna(method="bfill", axis=1).fillna(
        method="ffill", axis=1)
    std = data.rolling(window_len, axis=1, center=True).std().fillna(method="bfill", axis=1).fillna(method="ffill",
                                                                                                    axis=1)

    high = median + accepted_zscore * std
    low = median - accepted_zscore * std

    outlier = (data > high) | (data < low)

    data[outlier] = np.nan
    data = data.interpolate(axis=1, limit_area="inside", limit=150, limit_direction="backward")
    return data


def remove_outliers_from_batch(data, accepted=3, return_modification_count=False):
    pd_data = [pd.DataFrame(single) for single in data]
    z_score = [single.interpolate(axis=0).apply(zscore, axis=0) for single in pd_data]

    without_outliers_but_nans = [single[(z < accepted) & (z > -accepted)] for (single, z) in zip(pd_data, z_score)]

    without_outliers = [single.interpolate(axis=0, limit_direction='forward') for single in without_outliers_but_nans]

    return np.array([np.array(single) for single in without_outliers])


def normalize(data: pd.DataFrame, v_min=None, v_max=None):
    v_min = data.min().min() if v_min is None else v_min
    v_max = data.max().max() if v_max is None else v_max
    new = (data - v_min) / (v_max - v_min)
    return new, (v_min, v_max)