from numpy import zeros
from numpy import ones
from numpy import vstack
from numpy.random import randn
import tensorflow as tf
from keras.optimizers import Adam, Nadam
from keras.models import Sequential
from keras.layers import RNN, LSTM, InputLayer
from keras.layers import Dense
from keras.layers import Reshape
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from matplotlib import pyplot
import src.main.flows as fl
import pickle


# define the standalone discriminator model
def define_discriminator(in_shape=(256,2)):
    model = Sequential()
    model.add(LSTM(32, input_shape=in_shape))
    # model.add(Conv2D(64, (3,3), strides=(2, 2), padding='same', input_shape=in_shape))
    # model.add(LeakyReLU(alpha=0.2))
    # model.add(Dropout(0.4))
    # model.add(Conv2D(64, (3,3), strides=(2, 2), padding='same'))
    # model.add(LeakyReLU(alpha=0.2))			# elu zamiast leaky relu powinno byc lepsze
    # model.add(Dropout(0.4))
    # model.add(Flatten())
    model.add(Dense(512, activation='sigmoid'))
    model.add(Dense(512, activation='sigmoid'))
    model.add(Dense(512, activation='sigmoid'))
    model.add(Dense(1, activation='sigmoid'))
    # compile model
    opt = Nadam(lr=0.0002, beta_1=0.5)  # zmien na Nadam... u mnie byl duzo lepszy
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


# define the standalone generator model
def define_generator(latent_dim):
    in_shape = (256,2)
    model = Sequential()
    model.add(InputLayer(input_shape=in_shape))
    model.add(LSTM(32,input_shape= in_shape))
    model.add(Dense(512, activation='sigmoid'))
    model.add(Dense(512, activation='sigmoid'))
    model.add(Dense(512, activation='sigmoid'))
    model.add(Reshape((256,2)))
    # compile model
    opt = Adam(lr=0.0002, beta_1=0.5)  # zmien na Nadam... u mnie byl duzo lepszy
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

    return model


# define the combined generator and discriminator model, for updating the generator
def define_gan(g_model, d_model):
    # make weights in the discriminator not trainable
    d_model.trainable = False
    # d_model.build(input_shape=(32,2,256))
    # g_model.compile(loss='binary_crossentropy', optimizer=opt)
    # print(d_model.summary())
    # connect them
    model = Sequential()
    # add generator
    model.add(g_model)
    # add the discriminator
    model.add(d_model)
    # compile model
    opt = Nadam(lr=0.0002, beta_1=0.5)
    model.compile(loss='binary_crossentropy', optimizer=opt)
    return model


# load and prepare mnist training images
def load_real_samples():
    df_hr, df_mbp = fl.load_data()
    # remove outliers
    df_hr = fl.remove_outliers(df_hr)
    df_mbp = fl.remove_outliers(df_mbp)

    # normalize
    df_hr, _ = fl.normalize(df_hr, pd.DataFrame.min(df_hr), pd.DataFrame.max(df_hr))
    df_mbp, _ = fl.normalize(df_mbp, pd.DataFrame.min(df_mbp), pd.DataFrame.max(df_mbp))

    return df_hr.iloc[:50], df_mbp.iloc[:50]  # wzięcie 50 pierwszych pacjentów


# select real samples

def generate_fake_samples(gen_model):
    gen_model_input_shape = (256,2)
    noise_shape = (32, *gen_model_input_shape)
    noise = np.random.random(noise_shape)

    fake_samples = gen_model.predict(noise)
    return (fake_samples, np.zeros([32,1]))


# generate points in latent space as input for the generator
def generate_latent_points(latent_dim, n_samples):
    # generate points in the latent space
    x_input = randn(latent_dim * n_samples)
    # reshape into a batch of inputs for the network
    x_input = x_input.reshape(n_samples, latent_dim)
    return x_input


# use the generator to generate n fake examples, with class labels
# def generate_fake_samples(g_model, latent_dim, n_samples):
#     # generate points in latent space
#     x_input = generate_latent_points(latent_dim, n_samples)
#     # predict outputs
#     X = g_model.predict(x_input)
#     # create 'fake' class labels (0)
#     y = zeros((n_samples, 1))
#     return X, y
#

# create and save a plot of generated images (reversed grayscale)
def save_plot(examples, epoch, n=10):
    # plot images
    for i in range(n * n):
        # define subplot
        pyplot.subplot(n, n, 1 + i)
        # turn off axis
        pyplot.axis('off')
        # plot raw pixel data
        pyplot.imshow(examples[i, :, :, 0], cmap='gray_r')
    # save plot to file
    filename = 'generated_plot_e%03d.png' % (epoch + 1)
    pyplot.savefig(filename)
    pyplot.close()


# evaluate the discriminator, plot generated images, save generator model
def summarize_performance(epoch, g_model, d_model, dataset, latent_dim, n_samples=100):
    # prepare real samples

    X_real, y_real = generate_real_samples(dataset)  # tutaj test set~!
    # evaluate discriminator on real examples
    _, acc_real = d_model.evaluate(X_real, y_real, verbose=0)
    # prepare fake examples
    x_fake, y_fake = generate_fake_samples(g_model, latent_dim, n_samples)
    # evaluate discriminator on fake examples
    _, acc_fake = d_model.evaluate(x_fake, y_fake, verbose=0)
    # summarize discriminator performance
    print('>Accuracy real: %.0f%%, fake: %.0f%%' % (acc_real * 100, acc_fake * 100))
    # save plot
    save_plot(x_fake, epoch)
    # save the generator model tile file
    filename = 'generator_model_%03d.h5' % (epoch + 1)
    g_model.save(filename)


# train the generator and discriminator
def train(g_model, d_model, gan_model, dataset, latent_dim, n_epochs=100, n_batch=256):
    bat_per_epo =100
    half_batch = int(n_batch / 2)
    # manually enumerate epochs
    real_data_gen = generate_real_samples(dataset)
    for i in range(n_epochs):
        # enumerate batches over the training set
        for j in range(bat_per_epo):  # step
            # get randomly selected 'real' samples
            X_real, y_real = next(real_data_gen)
            # generate 'fake' examples
            X_fake, y_fake = generate_fake_samples(g_model)
            # create training set for the discriminator
            X, y = vstack((X_real, X_fake)), vstack((y_real, y_fake))
            # update discriminator model weights
            d_loss, _ = d_model.train_on_batch(X, y)
            # update the generator via the discriminator's error
            # X_gan, y_gan = generate_fake_samples(g_model)  # TODO
            g_loss = gan_model.train_on_batch(X_fake, y_fake)
            # summarize loss on this batch
            print('>%d, %d/%d, d=%.3f, g=%.3f' % (i + 1, j + 1, bat_per_epo, d_loss, g_loss))
        # evaluate the model performance, sometimes
        if (i + 1) % 10 == 0:
            summarize_performance(i, g_model, d_model, dataset, latent_dim)


from typing import Tuple, List, Union
import pandas as pd
import numpy as np


class EndOfSeriesException(BaseException):
    pass


# TODO -> error if remove_outlier 2x -> None + int error...

class Generator:
    """
    Object of this class generates fixed len batches of series from pandas DataFrame object.
    """

    def __init__(self, data: Union[pd.DataFrame, List], window_size=256, batch_size=1, batch_info=False):
        self.data = [data, ] if isinstance(data, pd.DataFrame) else data  # TODO -> eliminate checking type
        self.window_size = window_size
        self.index = 0
        self.row = 0
        self.current_series = None
        self.batch_size = batch_size
        self.batch_info = batch_info
        self._get_current_series()

    def __iter__(self):
        return self

    def __next__(self) -> np.array:
        return self._generate_batch()

    def _generate_sample(self) -> np.array:
        while True:
            start, stop = self._get_start_stop_indexes()

            if not self._check_indexes(stop):
                raise EndOfSeriesException()

            self.index += 1
            return np.array([single_series[start:stop].values for single_series in self.current_series]).T

    def _generate_batch(self) -> np.array:
        while True:
            batch = []
            for i in range(self.batch_size):
                try:
                    batch.append(self._generate_sample())
                except EndOfSeriesException:
                    self._move_to_the_next_series()
                    break  # for
                except Exception as e:
                    print(e)
                    raise
            else:
                if not self.batch_info:
                    return np.array(batch)
                else:
                    return np.array(batch), self.batch_info

    def _get_current_series(self) -> None:
        try:
            self.current_series = [single_data.iloc[self.row] for single_data in self.data]

            if self.batch_info:
                self.batch_info = self.data[0].index[self.row]
        except IndexError:
            raise StopIteration

        self._analyse_current_series()
        self._preprocess_series()

    def _preprocess_series(self):
        self._remove_nans_from_the_end_of_series()
        self._remove_nans_from_the_beginning_of_series()
        self._set_proper_len_of_series()

    def _get_start_stop_indexes(self) -> Tuple[int, int]:
        return self.index, self.index + self.window_size

    def _remove_nans_from_the_end_of_series(self) -> None:
        self.current_series = [single_series[:self.end_of_current_series + 1] for single_series in self.current_series]

    def _remove_nans_from_the_beginning_of_series(self) -> None:
        self.current_series = [single_series[self.start_of_current_series:] for single_series in self.current_series]

    def _pad_series_to_match_batch_size(self, len_of_remaining_series: int) -> None:
        padding_len = self.batch_size - len_of_remaining_series
        self.current_series = [pd.Series([0] * padding_len).append(single_series)
                               for single_series in self.current_series]

    def _cut_series_to_match_batch_size(self, start: int) -> None:
        self.current_series = [single_series[start:] for single_series in self.current_series]

    def _set_proper_len_of_series(self) -> None:
        num_of_windows = self.current_series[0].shape[0] - self.window_size + 1
        num_of_batches, rest = divmod(num_of_windows, self.batch_size)

        if num_of_batches < 0:
            return  # do nothing - checking indexes will trigger moving to the next series

        self._cut_series_to_match_batch_size(rest)

    def _check_indexes(self, stop_idx: int) -> bool:
        return all(stop_idx <= len(single_series) for single_series in self.current_series)

    def _move_to_the_next_series(self) -> None:
        self.row += 1
        self.index = 0
        self._get_current_series()

    def _analyse_current_series(self):
        try:
            self.end_of_current_series = min(single_series.last_valid_index()
                                             for single_series in self.current_series)
            self.start_of_current_series = max(single_series.first_valid_index()
                                               for single_series in self.current_series)
        except TypeError:  # if one of item is None (there is no valid index)
            self.end_of_current_series = -1
            self.start_of_current_series = -1


class XXGenerator:
    def __init__(self, data, *args, **kwargs):
        self.data = Generator(data, *args, **kwargs)

    def __iter__(self):
        return self

    def __next__(self):
        item = next(self.data)

        while not np.isfinite(item.ravel()).all():
            item = next(self.data)
            print("OMMITED")
        return (item, item)

def generate_real_samples(dataset):
    # geneator ma w sobie juz dfs
    # df_hr_com, df_mbp_com = load_real_samples();
    # data = [df_hr_com,df_mbp_com]
    # gen = Generator(data = [df_hr_com, df_mbp_com],batch_size=2,window_size=256)  # + parametry //TODO czy +1 jeszcze dodać?
    #
    # # generacja prawdziwych sampli
    # X, y = generate_real_samples(gen)
    # X.shape = (2, 2, 256)  # y.shape=batch size //TODO czy taki miał być reshape
    # y.shape = (2)  # y.shape=batch size //TODO mam zapisane y.shape = batch size - czegoś chyba brakuje

    while True:
        generator = Generator(dataset, batch_size=32, window_size=256)
        for samples in generator:
            yield (samples, np.ones([samples.shape[0],1]))


# size of the latent space
latent_dim = (100,)
# create the discriminator
d_model = define_discriminator()
pickle.dump(d_model,open("../../models/example_model.h5",'wb'))
# d_model.save("C:\\Users\\Michal\\Desktop\\projektMichal\\magisterka\\models\\example_model.h5")
# create the generator
g_model = define_generator(latent_dim)
# create the gan
gan_model = define_gan(g_model, d_model)
# load image data
dataset = load_real_samples()
# train model
train(g_model, d_model, gan_model, dataset, latent_dim)

