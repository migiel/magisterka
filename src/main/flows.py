from keras.models import load_model
from sklearn.metrics import mean_squared_error
from src.main.common.data_handler import DataFrameCutter
from src.main.common.preprocessing import normalize, remove_outliers
from src.main.common.generator import XXGenerator

import pandas as pd
import numpy as np


def validate_model_on_data(model_path, df_paths=None, min_max=None, train_data=None, window_len=256,
                           rtn="error", threshold=None):
    print("Load model...")
    if type(model_path) is str:
        model = load_model(model_path)
    else:
        model = model_path

    print("Prepare data...")

    min_max = []
    if train_data is not None:
        for d in train_data:

            if type(d) is str:
                d = pd.read_pickle(d)

            _, mm = normalize(d)
            min_max.append(mm)

    data = []
    for n, df in enumerate(df_paths):

        if type(df) is str:
            single_df = pd.read_pickle(df)
        else:
            single_df = df

        single_df = remove_outliers_complete(single_df)
        if train_data is not None:
            single_df, _ = normalize(single_df, v_min=min_max[n][0],
                                     v_max=min_max[n][1])  # TODO -> add min + max from train data
        else:
            single_df, _ = normalize(single_df)
        data.append(single_df)

    test = XXGenerator(data, window_size=window_len, batch_size=256)
    print("Validate errors...")

    error = []
    predicted = []

    for (x, y) in test:
        pred = model.predict(x)
        predicted.append(pred)

        if rtn == "error":
            for t, p in zip(x, pred):
                error.append(mean_squared_error(t, p))

    if rtn == "prediction":
        return predicted
    return error


def get_samples_from_df(df, end_offset, num_of_samples, window_len):
    data = []

    for no, i in enumerate(df[0].index):
        for n in range(num_of_samples):
            single_data = []
            for single in df:
                series = single.loc[i]

                end = series.last_valid_index() - end_offset - n + 1
                start = end - window_len

                single_data.append(series[start:end])

            data.append(np.array(single_data).reshape(len(df), window_len))
    print(data)
    adata = np.array(data)
    print(adata.shape)
    return np.transpose(np.array(data), axes=(0, 2, 1))


def train_model(model, df_paths, epochs, steps_per_epoch, window_len=256, validation_df_paths=None):
    print("Prepare data...")

    data = []
    val_data = []
    min_max = []

    for df in df_paths:
        if type(df) is str:
            single_df = pd.read_pickle(df)
        else:
            single_df = df

        single_df = remove_outliers_complete(single_df)
        single_df, mm = normalize(single_df)
        data.append(single_df)
        min_max.append(mm)

    for n, df in enumerate(validation_df_paths):
        if type(df) is str:
            single_df = pd.read_pickle(df)
        else:
            single_df = df

        single_df = remove_outliers_complete(single_df)
        single_df, _ = normalize(single_df, v_min=min_max[n][0], v_max=min_max[n][1])
        val_data.append(single_df)

    print("Train model...")

    errors = []

    for i in range(epochs):
        data_gen = XXGenerator(data, window_size=window_len, batch_size=256)
        val_data_gen = XXGenerator(val_data, window_size=window_len, batch_size=256)
        try:
            model.fit_generator(data_gen,
                                steps_per_epoch=steps_per_epoch,
                                epochs=i + 1,
                                initial_epoch=i,
                                validation_data=val_data_gen,
                                validation_steps=steps_per_epoch // 3,
                                )
        except Exception as e:
            print(e)
        errors.append((model.history.history["loss"], model.history.history["val_loss"]))

    return model, errors


def remove_outliers_complete(data, v_min=30, v_max=180):
    wo = data.copy()
    wo[(wo > v_max) | (wo < v_min)] = np.nan
    wo = remove_outliers(wo, accepted_zscore=5., window_len=128)
    wo = remove_outliers(wo, accepted_zscore=4., window_len=256)
    wo = remove_outliers(wo, accepted_zscore=3., window_len=256)
    wo = remove_outliers(wo, accepted_zscore=2.9, window_len=256)
    wo = remove_outliers(wo, accepted_zscore=2.9, window_len=384)

    return wo


def validate_model_on_specyfic_data(model, df_paths=None, min_max=None, train_data=None, window_len=256,
                                    num_of_samples=30, end_offset=60, rtn="error", threshold=None):
    from keras.models import load_model
    from sklearn.metrics import mean_squared_error

    print("Load model...")
    if model is str:
        model = load_model(model)

    print("Prepare data...")

    min_max = []
    if train_data is not None:
        for d in train_data:
            if type(d) is str:
                d = pd.read_pickle(d)
            _, mm = normalize(d)
            min_max.append(mm)

    data = []
    for n, df in enumerate(df_paths):

        if type(df) is str:
            single_df = pd.read_pickle(df)
        else:
            single_df = df

        single_df = remove_outliers_complete(single_df)
        if train_data is not None:
            single_df, _ = normalize(single_df, v_min=min_max[n][0],
                                     v_max=min_max[n][1])  # TODO -> add min + max from train data
        else:
            single_df, _ = normalize(single_df)
        data.append(single_df)

    d = get_samples_from_df(data, end_offset=end_offset,
                            num_of_samples=num_of_samples,
                            window_len=window_len)

    error = []
    predicted = model.predict(d)

    if rtn == "prediction":
        return predicted

    for t, p in zip(d, predicted):
        print(np.isnan(t.ravel()).any())
        if np.isnan(t.ravel()).any():
            continue
        error.append(mean_squared_error(t, p))

    if rtn == "error":
        return error
    elif rtn == "d+p":
        return d, predicted
    elif rtn == "anomaly" and threshold is not None:
        return np.array(error) > threshold


def load_data(syncopy=False):
    if syncopy:
        hr = pd.read_pickle("../resources/data/HR_SYNCOPY.pickle")
        mbp = pd.read_pickle("../resources/data/MBP_SYNCOPY.pickle")
        markers = pd.read_pickle("../resources/data/MARKERS_SYNCOPY.pickle")
        indexes = pd.read_csv("../resources/data/Indexes/syncopy_indexes.csv",
                              index_col=0, header=None)
    else:
        hr = pd.read_pickle("../resources/data/HR_NO_SYNCOPY.pickle")
        mbp = pd.read_pickle("../resources/data/MBP_NO_SYNCOPY.pickle")
        markers = pd.read_pickle("../resources/data/MARKERS_NO_SYNCOPY.pickle")
        indexes = pd.read_csv("../resources/data/Indexes/no_syncopy_indexes.csv",
                              index_col=0, header=None)

    hr = hr.loc[[i[0] for i in indexes.values]]
    mbp = mbp.loc[[i[0] for i in indexes.values]]

    hrc = DataFrameCutter(hr, markers=markers)
    mbpc = DataFrameCutter(mbp, markers=markers)

    hr_cutted = hrc.cut()
    mbp_cutted = mbpc.cut()

    return hr_cutted, mbp_cutted


def get_encoder(ae):
    def get_units(layer):
        try:
            return layer.units
        except AttributeError:
            return 0

    encoder_layers = [ae.layers[0]]
    for layer_nr in range(len(ae.layers)):
        if (get_units(ae.layers[layer_nr]) >= get_units(ae.layers[layer_nr + 1])
                or get_units(ae.layers[layer_nr]) == 0):
            encoder_layers.append(ae.layers[layer_nr + 1])
        else:
            break

    return Sequential(encoder_layers)


def show_metrics(predictions):
    from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, roc_auc_score

    true90 = np.append(np.ones_like(predictions["NO SYNC"]), np.zeros_like(predictions["SYNC 120-90"]), axis=0) * 2 - 1
    true60 = np.append(np.ones_like(predictions["NO SYNC"]), np.zeros_like(predictions["SYNC 90-60"]), axis=0) * 2 - 1
    true45 = np.append(np.ones_like(predictions["NO SYNC"]), np.zeros_like(predictions["SYNC 60-45"]), axis=0) * 2 - 1
    true30 = np.append(np.ones_like(predictions["NO SYNC"]), np.zeros_like(predictions["SYNC 45-30"]), axis=0) * 2 - 1

    pred90 = np.append(predictions["NO SYNC"], predictions["SYNC 120-90"], axis=0)
    pred60 = np.append(predictions["NO SYNC"], predictions["SYNC 90-60"], axis=0)
    pred45 = np.append(predictions["NO SYNC"], predictions["SYNC 60-45"], axis=0)
    pred30 = np.append(predictions["NO SYNC"], predictions["SYNC 45-30"], axis=0)

    for offset, banner in [(90, "120-90sec"), (60, "90-60sec"), (45, "60-45sec"), (30, "45-30sec")]:
        print("#" * 30, banner, "#" * 30)
        for m in [confusion_matrix, accuracy_score, roc_auc_score, f1_score]:
            print(m.__name__)
            print(m(locals()["true{}".format(offset)], locals()["pred{}".format(offset)]), "\n")


def predict_custom_model(model):
    sync_hr, sync_mbp = load_data(syncopy=True)
    non_sync_hr, non_sync_mbp = load_data(syncopy=False)

    sync_hr_test = sync_hr.iloc[56:]
    sync_mbp_test = sync_mbp.iloc[56:]

    sync_hr_train = sync_hr.iloc[:56]
    sync_mbp_train = sync_mbp.iloc[:56]

    non_sync_hr_test = non_sync_hr.iloc[56:]
    non_sync_mbp_test = non_sync_mbp.iloc[56:]

    non_sync_hr_train = non_sync_hr.iloc[:56]
    non_sync_mbp_train = non_sync_mbp.iloc[:56]

    e_120_90 = validate_model_on_specyfic_data(model, (sync_hr_test, sync_mbp_test),
                                               train_data=(non_sync_hr_train, non_sync_mbp_train), num_of_samples=30,
                                               end_offset=90, rtn="prediction")

    e_90_60 = validate_model_on_specyfic_data(model, (sync_hr_test, sync_mbp_test),
                                              train_data=(non_sync_hr_train, non_sync_mbp_train), num_of_samples=30,
                                              end_offset=60, rtn="prediction")

    e_60_45 = validate_model_on_specyfic_data(model, (sync_hr_test, sync_mbp_test),
                                              train_data=(non_sync_hr_train, non_sync_mbp_train), num_of_samples=15,
                                              end_offset=45, rtn="prediction")

    e_45_30 = validate_model_on_specyfic_data(model, (sync_hr_test, sync_mbp_test),
                                              train_data=(non_sync_hr_train, non_sync_mbp_train), num_of_samples=15,
                                              end_offset=30, rtn="prediction")

    no_sync_err = validate_model_on_data(model, (non_sync_hr_test, non_sync_mbp_test),
                                         train_data=(non_sync_hr_train, non_sync_mbp_train), rtn="prediction")

    data = {"NO SYNC": np.array(no_sync_err).reshape(-1),
            "SYNC 120-90": np.array(e_120_90).reshape(-1),
            "SYNC 90-60": np.array(e_90_60).reshape(-1),
            "SYNC 60-45": np.array(e_60_45).reshape(-1),
            "SYNC 45-30": np.array(e_45_30).reshape(-1)}
    return data