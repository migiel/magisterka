from typing import Tuple, List, Union
import pandas as pd
import numpy as np


class EndOfSeriesException(BaseException):
    pass


# TODO -> error if remove_outlier 2x -> None + int error...

class Generator:
    """
    Object of this class generates fixed len batches of series from pandas DataFrame object.
    """

    def __init__(self, data: Union[pd.DataFrame, List], window_size=256, batch_size=1, batch_info=False):
        self.data = [data, ] if isinstance(data, pd.DataFrame) else data  # TODO -> eliminate checking type
        self.window_size = window_size
        self.index = 0
        self.row = 0
        self.current_series = None
        self.batch_size = batch_size
        self.batch_info = batch_info
        self._get_current_series()

    def __iter__(self):
        return self

    def __next__(self) -> np.array:
        return self._generate_batch()

    def _generate_sample(self) -> np.array:
        while True:
            start, stop = self._get_start_stop_indexes()

            if not self._check_indexes(stop):
                raise EndOfSeriesException()

            self.index += 1
            return np.array([single_series[start:stop].values for single_series in self.current_series]).T

    def _generate_batch(self) -> np.array:
        while True:
            batch = []
            for i in range(self.batch_size):
                try:
                    batch.append(self._generate_sample())
                except EndOfSeriesException:
                    self._move_to_the_next_series()
                    break  # for
                except Exception as e:
                    print(e)
                    raise
            else:
                if not self.batch_info:
                    return np.array(batch)
                else:
                    return np.array(batch), self.batch_info

    def _get_current_series(self) -> None:
        try:
            self.current_series = [single_data.iloc[self.row] for single_data in self.data]

            if self.batch_info:
                self.batch_info = self.data[0].index[self.row]
        except IndexError:
            raise StopIteration

        self._analyse_current_series()
        self._preprocess_series()

    def _preprocess_series(self):
        self._remove_nans_from_the_end_of_series()
        self._remove_nans_from_the_beginning_of_series()
        self._set_proper_len_of_series()

    def _get_start_stop_indexes(self) -> Tuple[int, int]:
        return self.index, self.index + self.window_size

    def _remove_nans_from_the_end_of_series(self) -> None:
        self.current_series = [single_series[:self.end_of_current_series + 1] for single_series in self.current_series]

    def _remove_nans_from_the_beginning_of_series(self) -> None:
        self.current_series = [single_series[self.start_of_current_series:] for single_series in self.current_series]

    def _pad_series_to_match_batch_size(self, len_of_remaining_series: int) -> None:
        padding_len = self.batch_size - len_of_remaining_series
        self.current_series = [pd.Series([0] * padding_len).append(single_series)
                               for single_series in self.current_series]

    def _cut_series_to_match_batch_size(self, start: int) -> None:
        self.current_series = [single_series[start:] for single_series in self.current_series]

    def _set_proper_len_of_series(self) -> None:
        num_of_windows = self.current_series[0].shape[0] - self.window_size + 1
        num_of_batches, rest = divmod(num_of_windows, self.batch_size)

        if num_of_batches < 0:
            return  # do nothing - checking indexes will trigger moving to the next series

        self._cut_series_to_match_batch_size(rest)

    def _check_indexes(self, stop_idx: int) -> bool:
        return all(stop_idx <= len(single_series) for single_series in self.current_series)

    def _move_to_the_next_series(self) -> None:
        self.row += 1
        self.index = 0
        self._get_current_series()

    def _analyse_current_series(self):
        try:
            self.end_of_current_series = min(single_series.last_valid_index()
                                             for single_series in self.current_series)
            self.start_of_current_series = max(single_series.first_valid_index()
                                               for single_series in self.current_series)
        except TypeError:  # if one of item is None (there is no valid index)
            self.end_of_current_series = -1
            self.start_of_current_series = -1


class XXGenerator:
    def __init__(self, data, *args, **kwargs):
        self.data = Generator(data, *args, **kwargs)

    def __iter__(self):
        return self

    def __next__(self):
        item = next(self.data)

        while not np.isfinite(item.ravel()).all():
            item = next(self.data)
            print("OMMITED")
        return (item, item)