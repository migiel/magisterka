#!/bin/bash -l
#SBATCH --ntasks=1
#SBATCH --time=72:00:00
#SBATCH -A fallprevention2
#SBATCH -p plgrid
#SBATCH --output="fresh.out"
#SBATCH --error="error.out"

module add plgrid/tools/python/3.6.5
module add plgrid/libs/python-scipy/1.0.1-python-3.6
module add plgrid/libs/tensorflow-cpu/1.9.0-python-3.6
module add plgrid/libs/python-numpy/1.14.2-python-3.6

an=1
MAG="magisterka${an}"
ne=40
opt="nadam"
lrD=0.00003
lrG=0.000005
loss="binary_crossentropy"
dr=1
sl=False
gs=3

cd $SLURM_SUBMIT_DIR
cd /net/people/plgmigiel/$MAG

srun python3.6 /net/people/plgmigiel/$MAG/mainGan.py $ne $opt $lrD $lrG $loss $dr $sl $gs
cd ../
python2 mailsend.py $an
