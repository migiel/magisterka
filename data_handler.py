import numpy as np
from scipy.io import loadmat
import os
import pandas as pd


class DataHandler:
    def __init__(self, path):
        self.path = path
        self.parsed_files = {}

        self.__find_files()

    def __find_files(self):
        files = os.listdir(self.path)
        self.files = [os.path.join(self.path, file) for file in files]

    def parse_all_files(self, names):
        if names not in self.parsed_files.keys():
            data = {}
            for file in self.files:
                f = os.path.basename(file).partition(".")[0]
                data[f] = (self.get_single_sample(f, names))
                print("|", end="")

            self.parsed_files.update({names: data})

        return self.parsed_files[names]

    def get_data_frame(self, names):
        df = pd.DataFrame()
        markers = pd.DataFrame()

        if self.parsed_files.get(names) is None:
            self.parse_all_files(names)

        for k, v in self.parsed_files[names].items():
            x = pd.DataFrame(data=v[0].T, index=[k])
            m = pd.DataFrame(data=v[1], columns=[k])
            df = pd.concat((df, x), axis=0)
            markers = pd.concat((markers, m), axis=1)

        markers = markers.T

        return df, markers

    def get_single_sample(self, file_name, data_names):
        file_path = os.path.join(self.path, file_name)
        m_file = loadmat(file_path)

        mdata = m_file[data_names[0]]
        mdtype = mdata.dtype
        ndata = {n: mdata[n][0, 0] for n in mdtype.names}
        index = 0
        data = []
        while True:
            try:
                data.append(ndata[data_names[1]][index][0])
                index += 1
            except Exception as e:
                print(e)
                break

        markers = [x.shape[1] for x in data]
        markers = np.cumsum(markers)

        data = np.concatenate(data, axis=1).T

        return data, markers


class DataFrameCutter:
    def __init__(self, data: pd.DataFrame, markers: pd.DataFrame):
        self.data = data
        self.markers = markers

        self.data_dict = None

    def cut(self):
        self.data_dict = {}
        for single_sample in self.data.index:
            markers = self.markers.loc[single_sample]
            try:
                start = int(markers[0])
                stop = int(markers[1])
            except (IndexError, ValueError) as e:
                start = 0
                stop = 0
                print(e)

            values = self.data.loc[single_sample][start:stop].values
            self.data_dict.update({single_sample: values})

        return self._convert_to_data_frame()

    def _convert_to_data_frame(self):
        df = pd.DataFrame()

        for k, v in self.data_dict.items():
            x = pd.DataFrame(data=[v], index=[k])
            df = pd.concat((df, x), axis=0)

        return df